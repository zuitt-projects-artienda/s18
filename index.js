// alert("hi");

//Javascript Objects
	// similar to an array, it also contains multiple valies
	// unlike an array that uses index, objects use properties.
	// with object, we can easily give labels to each value.

	// Structure or Syntax
	/*
		let objectName - {
			key : value,
			key : function,
			key : object{ key : value},
			key : [array]
		}
	*/

let cellphone = {
	name: "Nokie 3210",
	manufactureDate: 1999
};
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
	/*
		Syntax:

		function objectionName(keyA, keyB){
			//assign property of a parameter
			this.keyA = keyA;
			this.keyB = keyB;
		};
	*/

	function Laptop(name, manufactureDate){
			this.name = name;
			this.releaseDate = manufactureDate;

	}; 

	let laptop = new Laptop("Lenovo", 2008);
	console.log(laptop);
		//will create a new object
		// NEW operator creates an instance of an object,
	let laptop1 = Laptop("Asus", 2010);
	console.log(laptop1);
		//result: undfined

	//Creating empty objects
		let computer = {};
		let myComputer = new Object();
		console.log(computer);
		console.log(myComputer);

	// Accessing Array to Objects
		let machines = [laptop, laptop1];

		// access property of an object inside an array
		console.log(machines[0].name);
		console.log(machines[0]["releaseDate"]);
			//can access using double quotes

//Initializing / Adding / Deleting / Reassigning Object Properties

	let car = {}

	// initializing/ adding object properties using dot notation
	car.name = "Honda Civic";
	console.log(car);

	// adding object property with square brackets
	car["manufactureDate"] = 2019;
	console.log(car);

	// deleting object properties
	delete car["name"];
	console.log(car);

//Mini Activity
	car.brand = "Mitsubishi";
	car.model = "Mirage"
	car.color = "Gray";
	car.location = "Manila";

	console.log(car);

	// Reassining objects properties
	car.manufactureDate = 1986;
	console.log(car);

//OBJECT METHODS
	// a method is a function which is a property of an object
	// they are also functions and one of the key differences they have is that methods are functions related to a specificobject.
	// methods are defined based on what an objeect is capable of doing and how it should wok.

	let person = {
		name: "Jin",
		talk: function(){
			console.log(`Hello my name is ${this.name}`)
		}
	};
	person.talk();

	let friend = {
		firstName :"Bam",
		lastName :"Jeon",
		address : {
			city: "Caloocan",
			country: "Philippines"
		},
		emails: ["bam@mail.com", "jeonbam@gmail.com"],
		introduce: function(){
			console.log(`Hello my name is ${this.name} ${this.lastName}, I live in ${this.address.city}. My personal email is ${this.emails[1]}`)
		}
	};
		friend.introduce();


	// Real World Application

	let myPokemon = {
		name: "Bulbasaur",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log(`${this.name} tackled another pokemon`)
			console.log(`targetPokemon's healh is now reducted`)
		},
		faint: function(){
			console.log(`${this.name} fainted`)
		}
	};

		myPokemon.tackle();
		myPokemon.faint();


		//Object Constructor
		function Pokemon(name, lvl, hp){
			//Properties
			this.name = name;
			this.lvl = lvl;
			this.health = hp * 2
			this.attact = lvl;

			// methods
			this.tackle = function(target){
				console.log(`${this.name} tackled ${target.name}`);
				console.log(`targetPokemon's health is now reducued`);
				console.log(target.name);
				console.log(this.name);
			},
			this.faint = function (){
				console.log(`${this.name} fainted`)
			}
		};

		// create new instances of the Pokemon object each with their unique properties.

		let pikachu = new Pokemon ("Pikachue", 3, 50);
		let ratata = new Pokemon ("Ratata", 5, 25);

		//Providing the "ratata" object as an arguement to "pikachu" tackle method will create interaction between the two objects

		pikachu.tackle(ratata);
		ratata.tackle(pikachu);
